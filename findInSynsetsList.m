% -------------------------------------------------------------------------
% Returns 1 if temp is found in sysnets, else 0. All entries are assumed to
% be char type. 
% -------------------------------------------------------------------------
function found = findInSynsetsList(wnid, synsets)
    found = 0; 
    for i = 1:1:length(synsets)
        if (strcmp(wnid,synsets(i,:)))
            found = 1; 
        end  
    end