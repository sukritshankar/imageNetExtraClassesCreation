% -------------------------------------------------------------------------
% This file forms a comprehensive statistics of the entire (32K class)
% ImageNet dataset. It also gathers data for extra classes of ImageNet
% -------------------------------------------------------------------------
clc; clear; close all; 

%% Configuration Variables 
config.crossCheck = 0; % Cross Checks the finally selected synsets if they contain any of undersired ones
config.subtreeNodesThreshold = 20; % The number of nodes in the subtree above which the subtree will not be ommitted 
config.noOfClasses = 500; % Number of additional classes (apart from 1000 from ILSVRC) to consider 
config.processPart = 3; % 1 = Extract important information from Complete Imagenet Explorer Hierarchy 
                                %(.1 = Store wnid and no of images for all sysnets)
                                %(.2 = find ext set of all prohibited ids)
                          % 2 = Select classes 
                          % 3 = Download the images from the selected classes
config.downloadImages = 3; % 1 = Download images for each synset as tars, 2 = Untar all downloaded tars, 3 = download tars and untar
config.mainFolderName = 'new_classes_data'; 

%% Extract important information from Complete Imagenet Explorer Hierarchy 
if (floor(config.processPart) == 1)
    % ---------------------------------------------------------------------
    % Get all the Synset Ids of ImageNet which were considered for making
    % the ILSVRC Challenge - We load a slight superset of ILSVRC Ids,
    % which was considered for making the challenge. This helps us to omit
    % very similar images to ILSVRC Datasets
    load ('meta_clsloc.mat');  % Access WNID in the structure 
    synsetsToOmit = char(size(synsets,2),synsets(1,1).WNID,2); % These are the synsetIds which should not be there in our new dataset 
    for i = 1:1:size(synsets,2)
        synsetsToOmit(i,:) = synsets(1,i).WNID;   % All children of these nodes will also be ommitted 
    end
    clear synsets;
    
    % ---------------------------------------------------------------------
    % Get the tree from the XML File
    imageNetTREE = parseXML('imagenet_structure.xml'); 
    synsets = imageNetTREE.getElementsByTagName('synset');
    
    % ---------------------------------------------------------------------
    % Find Synsets with Unique WNIDs
    for i = 1:1:synsets.getLength - 1 
         wnid = synsets.item(i).getAttribute('wnid'); wnid = char(wnid);
         if (length(wnid) == 9)
            uniqueSynsetsWnIds(i,:) = wnid;
         end
         clear wnid; 
    end
    uniqueSynsetsWnIds = unique(uniqueSynsetsWnIds,'rows'); 
    
    % --------------------------------------------------------------
    % Find the number of images for each sysnet  and store 
    if (config.processPart == 1.1)
        synsetWNID = char(zeros(size(uniqueSynsetsWnIds,1),9)); 
        numberOfImages = zeros (size(uniqueSynsetsWnIds,1), 1); 
        for i = 1:1:size(uniqueSynsetsWnIds,1)
            n = findNumberOfImagesInSynset (uniqueSynsetsWnIds(i,:)); 

            % Store in a structure 
            synsetWNID(i,:) = uniqueSynsetsWnIds(i,:); 
            numberOfImages(i,1) = n; 

            % Print Progress
            fprintf ('\n Synset Information processed for %d',i); 
        end
        % Save the structure in a mat file since its a bit expensive process 
        save ('sysnetIdsAndNoOfImages.mat','synsetWNID','numberOfImages');
    end
    
    % --------------------------------------------------------------    
    % Find the ext set of prohibited WNIDs (includes subtrees of the main
    % synset nodes)
    if (config.processPart == 1.2)
        synsetsToOmitALL = synsetsToOmit; 
        for i = 1:1:size(uniqueSynsetsWnIds,1)
            wnid = uniqueSynsetsWnIds(i,:); 

            % If any sysnet wnid matches that of synsetsToOmit, also get the
            % wnids of the entire subtree for that node. The images for a node
            % do not contain the images for the child nodes; however, they
            % mostly are similar.
            if (findInSynsetsList(wnid, synsetsToOmit))
                fprintf ('--'); 
                % Find the WNIDs of all synsets in the subtree 
                subtreeWnIds =  listSubtreeWnIds (wnid); 
                if (size(subtreeWnIds,1) >= 1  && size(subtreeWnIds,1) <= config.subtreeNodesThreshold)
                    synsetsToOmitALL(end + 1 : end + size(subtreeWnIds,1), :)  = subtreeWnIds; 
                    fprintf ('\n Extending Prohibition for subtree of node = %s (%d) \n', wnid,i); 
                end
                
                % Clear variables
                clear subtreeWnIds; 
            end
        end
        
        synsetsToOmitALL = unique (synsetsToOmitALL,'rows');
        % Save the extended set 
        save ('synsetsToOmitALL.mat','synsetsToOmitALL'); 
    end
end

%% Select synsets 
if (config.processPart == 2)
    % Load the necessary files from Process 1 
    load ('synsetsToOmitALL.mat'); % synsetWNID, numberOfImages
    load ('sysnetIdsAndNoOfImages.mat'); % synsetsToOmitALL
    
    % Sort all the synsets according to the number of images 
    [sortedImageNumbers,sortedIndices] = sort (numberOfImages,'descend'); 
    synsetWNIDSorted = synsetWNID(sortedIndices,:); 
    
    % Find the difference set - Set which includes only the 'new' nodes
    [~,selectedIds] = setdiff(synsetWNIDSorted,synsetsToOmitALL,'rows');
     
    % Take the top config.noOfClasses
    k = 1; 
    for i = 1:1:size(synsetWNID,1)
       if (find (selectedIds == i))  
           finalSynsetWnIds (k,:) = synsetWNIDSorted(i,:); 
           finalImageNumbers(k,1) = sortedImageNumbers(i); 
           k = k + 1; 
           if (k > config.noOfClasses)
               break; 
           end
       end
    end
    
    % Once we have the final wnids, also select their words 
    % Get the tree from the XML File
    imageNetTREE = parseXML('imagenet_structure.xml'); 
    synsets = imageNetTREE.getElementsByTagName('synset');
    finalSynsetWords = cell (config.noOfClasses,1); 
    for i = 1:1:synsets.getLength - 1 
        for j = 1:1:config.noOfClasses 
             wnid = synsets.item(i).getAttribute('wnid'); wnid = char(wnid);
             if (strcmp(wnid,finalSynsetWnIds(j,:)))
                finalSynsetWords{j,1} = char(synsets.item(i).getAttribute('words')); 
             end
             clear wnid; 
        end
        
        fprintf ('\n Capturing words by sweepng through - %d',i); 
    end
    
    % Save the final selection 
    save ('finalSelection.mat','finalSynsetWnIds','finalImageNumbers','finalSynsetWords');    
end

%% Once the classes are selected, download their images
if (config.processPart == 3)
    % Load the final selection list 
    load ('finalSelection.mat'); % Loads finalSynsetWnIds,finalImageNumbers,finalSynsetWords
    
    % Cross check if the finally selected synset WNIDs are there in the
    % list of ommitted ones. 
    if (config.crossCheck == 1)
        load ('synsetsToOmitALL.mat');
        for i = 1:1:size(synsetsToOmitALL,1)
            for j = 1:1:size(finalSynsetWnIds,1)
                if (strcmp(finalSynsetWnIds(j,:),synsetsToOmitALL(i,:)))
                    fprintf ('\n ------------------- %s', finalSynsetWnIds(j,:)); 
                    pause (inf); 
                else 
                    fprintf ('\n ALL GOOD TILL NOW (%d, %d)', i,j); 
                end
            end
        end
        fprintf ('\n -------------------------------------'); 
        fprintf ('\n SELECTED SYNSETS ARE ALL GOOD'); 
        fprintf ('\n -------------------------------------'); 
    end

    % Download the images 
    if (config.downloadImages == 1)
        parfor i = 1:1:size(finalSynsetWnIds,1) % For all classes 
            downloadTars (finalSynsetWnIds(i,:),config.mainFolderName);
        end
    end
    if (config.downloadImages == 2)
        % Now untar all folders - Processing of images (resizing, selecting
        % only jpgs and RGB ones is handled while creating LMDBs)
        parfor i = 1:1:size(finalSynsetWnIds,1) % For all classes 
            untar(strcat(config.mainFolderName,'/',finalSynsetWnIds(i,:),'.tar'), strcat(config.mainFolderName,'/',finalSynsetWnIds(i,:))); 
            fprintf ('\n Untarring images for synset - %s',finalSynsetWnIds(i,:)); 
        end
    end
    
    if (config.downloadImages == 3)
        parfor i = 1:1:size(finalSynsetWnIds,1) % For all classes 
            downloadTars (finalSynsetWnIds(i,:),config.mainFolderName);
        end
        parfor i = 1:1:size(finalSynsetWnIds,1) % For all classes 
            untar(strcat(config.mainFolderName,'/',finalSynsetWnIds(i,:),'.tar'), strcat(config.mainFolderName,'/',finalSynsetWnIds(i,:))); 
            fprintf ('\n Untarring images for synset - %s',finalSynsetWnIds(i,:)); 
        end
    end
end