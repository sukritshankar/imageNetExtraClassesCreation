% -------------------------------------------------------------------------
% Returns the number of images in a synset - As listed in ImageNet
% Here, we need not recurse into the subtree 
% -------------------------------------------------------------------------
function n  = findNumberOfImagesInSynset (wnid)
    % Load the appropriate URL 
    URL = strcat('http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=',wnid); 
    
    % Get the contents in a temp file 
    options = weboptions('Timeout',Inf);
    websave('temp.txt',URL, options);
    
    % Parse the number of http:// in the file 
    temp = fileread ('temp.txt');
    q = strfind (temp,'http://'); 
    n = length(q); 
    clear temp q ; 
    
    
    % Delete the temporary file 
    % delete ('temp.txt'); 
