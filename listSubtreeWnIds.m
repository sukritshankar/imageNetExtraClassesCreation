% -------------------------------------------------------------------------
% List the WNIDs of the subtree of the wnid 
% -------------------------------------------------------------------------
function subtreeWnIds = listSubtreeWnIds (wnid)
    % Load the appropriate URL 
    URL = strcat('http://www.image-net.org/api/text/wordnet.structure.hyponym?wnid=',wnid,'&full=1'); 
    
    % Get the contents in a temp file 
    options = weboptions('Timeout',Inf);
    websave('wnids.txt',URL, options);
    
    % Get the Winids from the txt file
    subtreeWnIds = []; 
    k = 1; 
    fid = fopen ('wnids.txt');
    tline = fgetl(fid);
    while ischar(tline)
        tline = fgetl(fid);
        if (length(tline) > 1)
            subtreeWnIds(k,:) = tline(2:end);
            k = k + 1; 
        end
        
    end 
    fclose (fid); 
    
    % Delete the text file 
    % delete ('wnids.txt'); 
    
    
    