% -------------------------------------------------------------------------
% Downlaod tars of the wnids 
% Note that the registered username along with the issued access key has to
% be entered in the URL. I have entered my credentials here, which should
% work fine. One can also enter his own credentials.
% -------------------------------------------------------------------------
function downloadTars (wnid,folderPath)
    % In case the folderPath does not exist, make it 
    if (~exist(folderPath))
        mkdir (folderPath); 
    end
    
    fprintf ('\n Downloading all images for synset - %s',wnid); 
    URL = strcat('http://imagenet.stanford.edu/download/synset?wnid=',...
        wnid,'&username=sukritshankar&accesskey=b1aa5959c8ddf9136a75eb53f6ee1564e7bcaccc&release=latest&src=stanford'); 
    options = weboptions('Timeout',inf);
    websave(strcat(folderPath,'/',wnid,'.tar'),URL, options);