% -------------------------------------------------------------------------
% Parse XML File in its naive format. We do not convert into a MATLAB struct
% -------------------------------------------------------------------------
function tree = parseXML(filename)
    try
       tree = xmlread(filename);
    catch
       error('Failed to read XML file %s.',filename);
    end